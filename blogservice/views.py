from .models import Post, Replies
from django.views.generic import ListView, DetailView
from .forms import AddCommentForm
from django.views.generic.edit import FormMixin
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse
import json
from django.conf import settings
import urllib.parse
import urllib.request

# Create your views here.


class PostListView(ListView):
    template_name = 'blogservice/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

    def get_queryset(self):
        return Post.objects.filter(is_published=True, is_deleted=False).order_by('-date_posted')


class PostDetailView(FormMixin, DetailView):
    model = Post
    template_name = 'blogservice/post_details.html'
    context_object_name = 'detail'
    form_class = AddCommentForm

    def get_success_url(self):
        return reverse('blog-details', kwargs={'pk': self.object.id})

    def get_context_data(self, *args, **kwargs):
        context = super(PostDetailView, self).get_context_data(*args, **kwargs)
        replies = Replies.objects.filter(post=self.kwargs.get('pk'), is_approved=True).order_by('-date_posted')
        context['replies'] = replies
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # get the token submitted in the form
        # recaptcha_response = self.request.POST.get('g-recaptcha-response')
        # url = 'https://www.google.com/recaptcha/api/siteverify'
        # payload = {
        #     'secret': settings.RECAPTCHA_SECRET_KEY,
        #     'response': recaptcha_response
        # }
        # data = urllib.parse.urlencode(payload).encode()
        # req = urllib.request.Request(url, data=data)
        # # verify the token submitted with the form is valid
        # response = urllib.request.urlopen(req)
        # result = json.loads(response.read().decode())
        # # verify result gotten
        # if (not result['contact']) or (not result['action'] == ''):
        #     messages.error(self.request, 'Invalid reCAPTCHA. Please try again.')
        #     return super().form_invalid(form)

        cd = form.cleaned_data
        post = Post.objects.filter(id=self.kwargs.get('pk')).first()
        # check if user exists
        user = User.objects.filter(username=cd.get('name')).first()
        if not user:
            user = User.objects.create(username=cd.get('name'), password='Prodigy1234?', email=cd.get('email'),
                                       is_superuser=False, is_active=False, first_name=cd.get('name'))

        # save  guest user replies
        Replies.objects.create(comment=cd.get('comment'), post=post, posted_by=user)
        messages.info(self.request, "Comment successfully added. Your comment would be displayed after approval")
        return super(PostDetailView, self).form_valid(form)
