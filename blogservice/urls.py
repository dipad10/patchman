from django.urls import path
from . import views
from .views import PostListView, PostDetailView

urlpatterns = [
    path('', PostListView.as_view(), name='blog-main'),
    path('home/', PostListView.as_view(), name='blog-home'),
    path('post_details/<int:pk>/', PostDetailView.as_view(), name='blog-details'),
]