import uuid
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length=100)
    post_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    author = models.ForeignKey(User, related_name='user_post', on_delete=models.CASCADE)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    is_published = models.BooleanField(default=False)
    # for soft delete
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Replies(models.Model):
    post = models.ForeignKey(Post, related_name='replies', on_delete=models.CASCADE)
    comment_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    comment = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    posted_by = models.ForeignKey(User, related_name='user_replies', on_delete=models.CASCADE)
    # for moderation
    is_approved = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    # to display text string in shell
    def __str__(self):
        return self.comment
