from django.contrib import admin
from .models import Post, Replies

# Register your models here.
#To make post show up on admin side
admin.site.register(Post)
admin.site.register(Replies)