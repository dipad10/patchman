from django import forms
from .models import Replies


class AddCommentForm(forms.Form):
    email = forms.CharField(required=True,
                            widget=forms.TextInput(
                             attrs={'class': 'form-control'}))
    name = forms.CharField(required=True,
                           widget=forms.TextInput(
                             attrs={'class': 'form-control'}))
    comment = forms.CharField(required=True,
                              widget=forms.TextInput(
                                attrs={'class': 'form-control'}))