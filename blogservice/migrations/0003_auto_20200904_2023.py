# Generated by Django 3.1 on 2020-09-04 20:23

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('blogservice', '0002_auto_20200904_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='replies',
            name='comment_uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_uuid',
            field=models.UUIDField(default=uuid.uuid4),
        ),
    ]
